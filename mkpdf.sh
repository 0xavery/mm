#!/bin/sh
echo "idexing files"
sh ./indexer.sh

echo "generating western layout pdf"
convert @index-western.txt western.pdf

echo "generating manga layout pdf"
convert @index-manga.txt manga.pdf

echo "scaling pages"
pdfjam --paper letter -o western.pdf western.pdf
pdfjam --paper letter -o manga.pdf manga.pdf

echo "creating booklets"
pdfbook2 --paper=letter western.pdf
pdfbook2 --paper=letter manga.pdf

echo "cleaning up"
rm index-manga.txt index-western.txt 
mv *.pdf ./pdfs
