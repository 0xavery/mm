# Requirements:
a shit ton of LaTeX. Probably texlive*

## [manga-book.pdf](https://gitlab.com/0xavery/mm/-/blob/master/pdfs/manga-book.pdf)

booklet in a Right to Left, manga layout. This is not split into signatures

## [manga.pdf](https://gitlab.com/0xavery/mm/-/blob/master/pdfs/manga.pdf)

Letter sized pages, for reading Right to Left

## [western-book.pdf](https://gitlab.com/0xavery/mm/-/blob/master/pdfs/western-book.pdf)

booklet in a Left ot Right, western layout. This is not split into signatures

## [western.pdf](https://gitlab.com/0xavery/mm/-/blob/master/pdfs/western.pdf)

Letter sized pages, for reading Left to Right
